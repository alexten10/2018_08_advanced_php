<?php

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  <script>
    $(document).ready(function() {
      $('#page').hide().load('../pages/home.php #main', null, function() {
        $('#page').fadeIn();
        
        $('#nav li a').each(function() {
          $(this).click(function(e) {
            event.preventDefault(); //prevent to go the href link
            var page = $(this).attr('href');
            //console.log(page); // to see if we get the valie of href=
            $('#page').hide().load('../pages/' + page + ' #main');
            $('#page').fadeIn();
          });
        });
      });
    });
    
    
    
  </script>
  
</head>
<body>
  
  <?php include '../pages/inc/nav.inc.php'; ?>
  
  
  <div id="page"></div>
  
</body>
</html>

























