<?php

//process_form.php

function string ($value) {
  return preg_match('^([A-Za-z\s\-\']+)$/', $value);
}





if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  $errors = [];
  //print_r($_POST);
  
  $validator = [
    'first_name' => 'string',
    'last_name' => 'string',
    'email' => 'email',
    'password' => 'strength',
    'password_confirm' => 'match'
  ];
  
  // array_key_exists($key, $validator)
  
  //call_user_func($func_name, $params)
  
  foreach($_POST as $key => $value) {
    if(array_key_exists($key, $validator)) {
      
      //$errors[$key] = !call_user_func($validator[$key], $value); //1 will be for no errors, 0 fis error exists //same as lines below
      if(call_user_func($validator[$key], $value)) {
        $errors[$key] = 0;
      } else {
        $errors[$key] = 1;
      }//END if - else
    }//END if array
  }//END foreach
  
  header('Content-type: application/json');
  echo json_encode($errors);
}














