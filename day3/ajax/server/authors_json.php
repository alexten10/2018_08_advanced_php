<?php

//authors_json.php

$dbh = new PDO('sqlite:database1.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//if GET author id is set, select that author
if(!empty($_GET['author_id'])) {
  $query = "SELECT *
            FROM author
            WHERE author_id = :author_id";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':author_id', $_GET['author_id'], PDO::PARAM_INT);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);//1 result
  //var_dump($result);
} 

  else { //else select all authors
  $query = "SELECT *
            FROM author";
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $result = $stmt->fetchALL(PDO::FETCH_ASSOC);//many results
  //var_dump($result);
}
//END if

header('Content-type: application/json');

echo json_encode($result);
















