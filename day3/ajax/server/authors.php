<?php

//authors.php

$dbh = new PDO('sqlite:database1.sqlite');

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//if GET author id is set, select that author
if(!empty($_GET['author_id'])) {
  $query = "SELECT *
            FROM author
            WHERE author_id = :author_id";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':author_id', $_GET['author_id'], PDO::PARAM_INT);
  $stmt->execute();
  $author_info = $stmt->fetch(PDO::FETCH_ASSOC);//1 result
  //var_dump($author_info);
} 

  else { //else select all authors
  $query = "SELECT *
            FROM author";
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $authors = $stmt->fetchALL(PDO::FETCH_ASSOC);//many results
  //var_dump($authors);
}

?>



<?php if(!empty($author_info)) : ?><!-- if single author output detail in HTML -->
  
  <h1><?php echo $author_info['name'] ?></h1>
  <h2>Author country: <?php echo $author_info['country'] ?></h2>
  <p><img src="images/<?php echo $author_info['image'] ?>" alt="<?php echo $author_info['name'] ?>" /></p>
  <!-- the path to image src is related to authors.html file, not to this file (authors.php) -->

<?php else : ?><!-- else -->
  <?php foreach ($authors as $value) : ?><!-- output list in html -->
    <li data-id="<?php echo $value['author_id'] ?>" ><?php echo $value['name'] ?></li>
  <?php endforeach ?>

<?php endif ?><!-- endif -->





<!--  my variant made in class -->
<!-- 
  <?php foreach ($authors as $value) : ?>
  <li>Author: <?php echo $value['name'] ?><br/>
      Country: <?php echo $value['country'] ?><br/>
      <img src="../images/<?php echo $value['image'] ?>" alt="<?php echo $value['name'] ?>" />
  </li>
  <br/><br/>
  <?php endforeach ?>
-->




