<?php

//authors.php

$dbh = new PDO('sqlite:database1.sqlite');

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//if GET author id is set, select that author
if(!empty($_GET['author_id'])) {
  $query = "SELECT *
            FROM author
            WHERE author_id = :author_id";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':author_id', $_GET['author_id'], PDO::PARAM_INT);
  $stmt->execute();
  $author_info = $stmt->fetch(PDO::FETCH_ASSOC);//1 result
  //var_dump($author_info);
} 

  else { //else select all authors
  $query = "SELECT *
            FROM author";
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $authors = $stmt->fetchALL(PDO::FETCH_ASSOC);//many results
  //var_dump($authors);
}

?>


<!-- if single author output detail in HTML -->
<?php if(!empty($author_info)) : ?>
  
  <h1><?php echo $author_info['name'] ?></h1>
  <h2>Author country: <?php echo $author_info['country'] ?></h2>
  <p><img src="images/<?php echo $author_info['image'] ?>" alt="<?php echo $author_info['name'] ?>" /></p>

<!-- else -->
<?php else : ?>
<!-- output list in html -->
  <?php foreach ($authors as $value) : ?>
    <li data-id="<?php echo $value['author_id'] ?>" ><?php echo $value['name'] ?></li>
  <?php endforeach ?>

<!-- endif -->
<?php endif ?>





<!--  my variant made in class -->
<!-- 
  <?php foreach ($authors as $value) : ?>
  <li>Author: <?php echo $value['name'] ?><br/>
      Country: <?php echo $value['country'] ?><br/>
      <img src="../images/<?php echo $value['image'] ?>" alt="<?php echo $value['name'] ?>" />
  </li>
  <br/><br/>
  <?php endforeach ?>
-->




