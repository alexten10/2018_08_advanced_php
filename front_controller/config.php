<?php

//set error reporting
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

//define ('APP', __DIR__);

define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_DSN', 'mysql:host=localhost;dbname=booksite');


$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
//set error reporting connected with database
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require APP . '/lib/book_model.php';
require APP . '/lib/genre_model.php';



