<?php

// Genre model

/**
* Get list of genres for sidebar
* @param $dbh PDO database handle
* @return Array result
*/
function getGenres($dbh) {

	$query = "SELECT
				DISTINCT
				genre_id,
				name
				FROM
				genre
				JOIN book USING(genre_id)
				ORDER BY
				name ASC";

	$stmt = $dbh->prepare($query);

	$stmt->execute();

	// fetch multiple results
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

