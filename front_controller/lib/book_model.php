<?php

// Book model

/**
* Get random books for home page
* @param $dbh PDO database handle
* @param $limit Int Number of books
* @return Array result
*/
function getRandomBooks($dbh, $limit)
{
	$query = "SELECT
	book.title,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	book.book_id,
	book.description,
	author.name as author,
	author.author_id,
	genre.name as genre,
	format.name as format
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	ORDER BY RAND()
	LIMIT :limit";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);

	$stmt->execute();

	// fetch multiple books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	
}

/**
* Get single book by id
* @param $dbh PDO database handle
* @param $book_id Int Book id
* @return Array result
*/
function getBook($dbh, $book_id)
{
	$query = "SELECT
	book.title,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	book.book_id,
	book.description,
	author.name as author,
	author.author_id,
	author.country,
	book.in_print,
	genre.name as genre,
	format.name as format,
	publisher.name as publisher,
	publisher.city as city,
	publisher.publisher_id
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	WHERE
	book_id = :book_id";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':book_id', $book_id, PDO::PARAM_INT);

	$stmt->execute();

	// fetch one book
	return $stmt->fetch(PDO::FETCH_ASSOC);
}

/**
* Get books by publisher
* @param $dbh PDO database handle
* @param $publisher_id Int Publiser ID
* @param $limit Int Number of books
* @return Array result
*/
function getBooksByPublisher($dbh, $publisher_id, $limit = 3)
{
	$query = "SELECT
			  book.book_id,
			  book.image,
			  book.title
			  FROM
			  book
			  WHERE
			  publisher_id = :publisher_id
			  limit :limit";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':publisher_id', $publisher_id, PDO::PARAM_INT);
	$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);

	$stmt->execute();

	// fetch multiple books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);

}

/**
* Get books by author
* @param $dbh PDO database handle
* @param $author_id Int Author id
* @return Array result
*/
function getBooksByAuthor($dbh, $author_id)
{
	$query = "SELECT
	book.title,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	book.book_id,
	book.description,
	author.name as author,
	author.author_id,
	genre.name as genre,
	format.name as format
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	WHERE
	book.author_id = :author_id";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':author_id', $author_id, PDO::PARAM_INT);

	$stmt->execute();

	// fetch multiple books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	
}


/**
* Get books by genre
* @param $dbh PDO database handle
* @param $genre_id Int Genre id
* @return Array result
*/
function getBooksByGenre($dbh, $genre_id)
{
	$query = "SELECT
	book.title,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	book.book_id,
	book.description,
	author.name as author,
	author.author_id,
	genre.name as genre,
	format.name as format
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	WHERE
	book.genre_id = :genre_id";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':genre_id', $genre_id, PDO::PARAM_INT);

	$stmt->execute();

	// fetch multiple books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	
}


/**
* Get books by search
* @param $dbh PDO database handle
* @param $search String search term
* @return Array result
*/
function search($dbh, $search)
{
	$query = "SELECT
	book.title,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	book.book_id,
	book.description,
	author.name as author,
	author.author_id,
	genre.name as genre,
	format.name as format
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	WHERE
	MATCH(book.title)
	AGAINST(:search IN NATURAL LANGUAGE MODE)";

	$stmt = $dbh->prepare($query);

	$stmt->bindValue(':search', $search, PDO::PARAM_STR);

	$stmt->execute();

	// fetch multiple books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	
}
