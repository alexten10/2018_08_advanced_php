<?php


ob_start();
session_start();

define ('APP', __DIR__ . '/..'); //one folder up

require APP . '/config.php';

//var_dump($_GET);

//create allowed pages
$allowed = array (
            'about',
            'books',
            'detail',
            'contact', 
            'home', 
            'login', 
            'register'
            );


//if(empty($_GET['p'])) $_GET['p'] = 'home'; //same as below // if in 1 line you can ommit {}
if(empty($_GET['p'])) {
  $_GET['p'] = 'home';
}


// if page is allowed (from $allowed array)
if(in_array($_GET['p'], $allowed)) {
  //include page
  include APP . "/pages/" . $_GET['p'] . ".php";
} else {
  //include error page
  header('HTTP/1.0 404 Not Found'); //sets status of the page
  include APP . '/pages/error_404.php';
}




























