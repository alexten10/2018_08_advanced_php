<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=rand(1000,9000)?>" />
</head>
<body>

<div class="container">

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="/">home</a></li><li>
			<a href="/?p=books">books</a></li><li>
			<a href="/?p=about">about</a></li><li>
			<a href="/?p=contact">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->
