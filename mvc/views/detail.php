<?php

// load template files
include __DIR__ . '/inc/header.inc.php';
include __DIR__ . '/inc/banner.inc.php';
include __DIR__ . '/inc/search.inc.php';

?>

<h1><?php echo $title; ?></h1>

<?php include __DIR__ . '/inc/cart.inc.php'; ?>

  <?php include __DIR__ . '/inc/categories.inc.php'; ?>

  <div class="shelf">

    <div class="book_cover">

      <img src="images/covers/<?php echo $book['image']; ?>" alt="<?php echo $book['title']; ?>" />

    </div><!-- /.book_cover -->

    <div class="book_details">

      <h3><?php echo $book['title']; ?></h3>

      <ul>
        <li><strong>Title</strong>: <?php echo $book['title']; ?></li>
        <li><strong>Author</strong>: <?php echo $book['author']; ?></li>
        <li><strong>Genre</strong>: <?php echo $book['genre']; ?></li>
        <li><strong>Format</strong>: <?php echo $book['format']; ?></li>
        <li><strong>Number of Pages</strong>: <?php echo $book['num_pages']; ?></li>
        <li><strong>Year Published</strong>: <?php echo $book['year_published']; ?></li>
        <li><strong>In Print</strong>: <?php echo ($book['in_print']) ? 
          'yes' : 'no'; ?></li>
        <li><strong>Price</strong>: $<?php echo $book['price']; ?></li>
        <li><strong>Publisher</strong>: <?php echo $book['publisher']; ?></li>
        <li><strong>Publisher City</strong>: <?php echo $book['city']; ?></li>
        <li><form action="/?p=cart" method="post">
          <input type="hidden" name="book_id" value="<?=$book['book_id']?>" />
          <button type="submit">Add to Cart</button>
        </form></li>
      </ul>

    </div><!-- /.book_details -->

    <div class="book_author">

      <h4>Meet the author...</h4>

      <h5><?php echo $book['author'] ?></h5>

      <img src="images/authors/<?php echo $author; ?>.jpg" alt="<?php echo $book['author'] ?>" />

      <p><?php echo $book['author'] ?>.  Country: <?php echo $book['country'] ?></p>

      <img src="images/countries/<?php echo $flag ?>.jpg" alt="<?php echo $flag ?> Flag" />

      <p>View <a href="/?p=books&author_id=<?php echo $book['author_id'] ?>">other books by this author</a>.</p>

    </div><!-- /.book_author -->

    <div class="book_description">
            <h4>Description</h4>

      <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

    </div>

    <div class="book_publisher">

      <h3>Other books by this publisher</h3>

      <?php foreach($publisher_books as $row) : ?>
      <div class="book_item">
        <a href="/?p=detail&book_id=<?php echo $row['book_id']; ?>"><img src="images/covers/<?php echo $row['image']; ?>" alt="<?php echo $row['title']; ?>" /></a>
      </div>
    <?php endforeach; ?>

    </div>

    

  </div><!-- /.shelf -->

</div><!-- /.container -->

<?php include __DIR__ . '/inc/footer.inc.php'; ?>

