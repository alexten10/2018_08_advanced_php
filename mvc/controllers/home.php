<?php 

$title = 'Home';



// slides for hero image
$slides = array(
  'book_club.jpg',
  'books.jpg',
  'meet_the_author.jpg',
  'sf_releases.jpg'
);

// shuffle the slides
shuffle($slides);

// pick the top slide in the shuffled deck
$img = array_pop($slides);
//END of logic part


require APP . '/views/home.php';





