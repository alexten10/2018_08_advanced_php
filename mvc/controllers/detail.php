<?php 

$title = 'Book Details';

// if no book ID, send back to books page
if(empty($_GET['book_id'])) {
	header('Location: books.php');
	die;
}


// load models
require APP . '/models/book_model.php';
require APP . '/models/genre_model.php';

// get all the data we need to build the page
$book = getBook($dbh, $_GET['book_id']);
$genres = getGenres($dbh);
$publisher_books = getBooksByPublisher($dbh, $book['publisher_id']);

// use this for first part of flag image name
$flag = strtolower($book['country']);

// use this for first part of author image name
$author = strtolower(str_replace(' ', '_', $book['author']));

require APP . '/views/detail.php';