<?php


session_start();
ob_start(); 

define ('APP', __DIR__ . '/..');

require APP . '/config.php';
//echo 'it works'; // to check if we get to this point, then all code above works
//echo APP;
//echo __DIR__;

//define allowed routes
$allowed = array ('about', 'books', 'cart', 'detail', 'home', 'contact');


if(empty($_GET['p'])) {
  $_GET['p'] = 'home';
}

// load route controllers or load error controller
if(in_array($_GET['p'], $allowed)) {
  require APP . '/controllers/' . $_GET['p'] . '.php';
} else {
    die ('Error 404');
}

//flush output buffer









