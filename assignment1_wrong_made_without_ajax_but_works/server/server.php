<?php

//server.php
//open this file separately to check if $query works properly, all errors associated with database query will be shown on THIS page only

/*
//check if non- Ajax request comes, die with error message
$headers = getallheaders();
//var_dump($headers);
if (!array_key_exists('X-Requested-With', $headers)) {
  die ('wrong request');
}
*/

//same as above
//check if non- Ajax request comes, die with error message
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
  die('invalid request');
}


define ('APP', __DIR__ . '/..'); //one folder up
require APP . '/config.php'; // sets errors to be displayed (both php and DB errors), defines DB (+ user and password), connects to DB


if(!empty($_GET['book_id'])) { //if have GET request with book_id
  //get query to database for detail view about the book based on book_id
  $query = 'SELECT
            book.book_id,
            book.image,
            book.title,
            book.num_pages,
            book.price,
            book.year_published,
            book.in_print,
            author.name as author,
            publisher.name as publisher,
            format.name as format,
            genre.name as genre
            FROM
            book
            JOIN author USING(author_id)
            JOIN publisher USING(publisher_id)
            JOIN format USING(format_id)
            JOIN genre USING(genre_id)
            WHERE
            book.book_id = :book_id';
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':book_id', $_GET['book_id'], PDO::PARAM_INT);
  $stmt->execute();
  $book = $stmt->fetch(PDO::FETCH_ASSOC);// fetch one book info
}



//query for books list column
$query = 'SELECT
          book.book_id,
          book.title
          FROM
          book';

$stmt = $dbh->prepare($query);
$stmt->execute();
$books = $stmt->fetchAll(PDO::FETCH_ASSOC);//fetch multiple results

?>



<!-- START of HTML part -->
<!----------------------------------->


<?php if(!empty($book)) : ?><!-- START of conditon --><!-- if $book array is available -->
  <div id="detail_1">
    <h1 id="title"><?php echo $book['title'] ?></h1>
    <p id="image"><img src="images/<?php echo $book['image'] ?>" alt="<?php echo $book['title'] ?>" /><p>
  </div>
  
  <div id="detail_2">
    <ul>
      <li><strong>Title:</strong> <?php echo $book['title'] ?></li>
      <li><strong>Number of pages:</strong> <?php echo $book['num_pages'] ?></li>
      <li><strong>Price:</strong> <?php echo $book['price'] ?></li>
      <li><strong>Year published:</strong> <?php echo $book['year_published'] ?></li>
      <li><strong>In print:</strong> <?php echo ($book['in_print']) ? 'Yes' : 'No'; ?></li>
      <!-- <li><strong>In print:</strong> <?php echo $book['in_print'] ?></li> -->
      <li><strong>Author:</strong> <?php echo $book['author'] ?></li>
      <li><strong>Publisher:</strong> <?php echo $book['publisher'] ?></li>
      <li><strong>Format:</strong> <?php echo $book['format'] ?></li>
      <li><strong>Genre:</strong> <?php echo $book['genre'] ?></li>
    </ul>
  </div>
  

<?php else : ?>
  <table>
    <?php foreach ($books as $value) : ?><!-- loop through $books array for book titles in books list --><!-- :(colon) replaces {} -->
      <tr><td id="<?php echo $value['book_id'] ?>"><?php echo $value['title'] ?></td></tr><!-- use book_id number as id= for 'td' tag, this is used for sending request for detail view of the book-->
    <?php endforeach ?><!-- END loop foreach -->
  </table>


<?php endif ?><!-- END of condition -->

<!-------------------------------------->
<!-- END of HTML part -->








<!-- code ideas to find solution
  <?php //foreach ($book as $key => $value) : ?> 
    <?php //if($key !== 'book_id' || $key !== 'image') {
        //<li><strong><?php echo $key ?>:</strong> <?php //echo $value ?></li>
      } ?>
  <?php //endforeach ?>
-->


<!-- alternative way plasing loop results into a list
<?php //else : ?>
  <ul>
  <?php foreach ($books as $value) : ?>
    <li class="list_li" id="<?php echo $value['book_id'] ?>" ><?php echo $value['title'] ?></li>
  <?php endforeach ?>
  </ul>
-->



