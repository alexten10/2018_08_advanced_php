<?php


//category model


function allGenres($dbh)
{
  //create query
  $query = 'SELECT * FROM genre';
  //prepare query
  $stmt = $dbh->prepare($query);
  //execute query
  $stmt->execute();
  //fetch results
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
  
}