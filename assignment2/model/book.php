<?php
// book model


// 1 variant, for variant 2 with fixed book limit look at bottom
function getRandomBooks($dbh, $limit)
{
  $query = "SELECT
            book.book_id,
            book.title,
            book.image,
            book.num_pages,
            book.year_published,
            book.price,
            book.description,
            author.name as author,
            author.author_id,
#option              format.name as format, //didnt find the usage in book info
            genre.name as genre
            FROM
            book
            JOIN author USING(author_id)
            JOIN genre USING(genre_id)
#option              JOIN format UISNG(format_id)
            ORDER BY RAND()
            LIMIT :limit";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':limit', $limit, PDO::PARAM_INT); // comment it, if use commented lines above
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC); //fetch multiple books
}



// get a single book info by book_id
// author_id is needed for link "other books by this author"
# publisher_id is needed for bottom page section "other books by this publisher"
# publisher_id is sent to the getBookByPublisher function
function getBook($dbh, $book_id)
{
  $query = 'SELECT
            book.book_id,
            book.title,
            book.year_published,
            book.num_pages,
            book.in_print,
            book.price,
            book.image,
            book.description,
            author.name as author,
            author.author_id,
            author.country,
            genre.name as genre,
            format.name as format,
            publisher.name as publisher,
            publisher.city as city,
            publisher.publisher_id
            FROM
            book
            JOIN author USING(author_id)
            JOIN genre USING(genre_id)
            JOIN format USING(format_id)
            JOIN publisher USING(publisher_id)
            WHERE
            book.book_id = :book_id';
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':book_id', $book_id, PDO::PARAM_INT);
  $stmt->execute();
  // fetch one book
  return $stmt->fetch(PDO::FETCH_ASSOC);
}



// book.title is needed for alternate text of image <img alt= >
// also can be used as image popup title <img title= > when hover over the image
function getBookByPublisher($dbh, $publisher_id)
{
  $query = 'SELECT
            book.book_id,
            book.title,
            book.image
            FROM
            book
            JOIN publisher USING(publisher_id)
            WHERE
            book.publisher_id = :publisher_id';
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':publisher_id', $publisher_id, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



function getBooksByAuthor($dbh, $author_id)
{
  $query = "SELECT
            book.book_id,
            book.title,
            book.num_pages,
            book.year_published,
            book.price,
            book.image,
            book.description,
            author.name as author,
            author.author_id,
#              format.name as format,
            genre.name as genre
            FROM
            book
            JOIN author USING(author_id)
            JOIN publisher USING(publisher_id)
            JOIN genre USING(genre_id)
          #  JOIN format USING(format_id)
            WHERE
            book.author_id = :author_id";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':author_id', $author_id, PDO::PARAM_INT);
  $stmt->execute();
  // fetch multiple books
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



// author.author_id value is needed to send this id in 
// the request url (when click on author name) to get info about author
function getBooksByGenre($dbh, $genre_id)
{
  $query = "SELECT
            book.book_id,
            book.title,
            book.num_pages,
            book.year_published,
            book.price,
            book.image,
            book.description,
            author.name as author,
            author.author_id,
            genre.name as genre
        #    format.name as format //didnt find usage in the list results of books by genre 
            FROM
            book
            JOIN author USING(author_id)
            JOIN publisher USING(publisher_id)
            JOIN genre USING(genre_id)
        #    JOIN format USING(format_id)
            WHERE
            book.genre_id = :genre_id";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':genre_id', $genre_id, PDO::PARAM_INT);
  $stmt->execute();
  // fetch multiple books
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



function search($dbh, $search)
{
  $query = "SELECT
            book.title,
            book.num_pages,
            book.year_published,
            book.price,
            book.image,
            book.book_id,
            book.description,
            author.name as author,
            author.author_id,
            genre.name as genre,
            format.name as format
            FROM
            book
            JOIN author USING(author_id)
            JOIN publisher USING(publisher_id)
            JOIN genre USING(genre_id)
            JOIN format USING(format_id)
            WHERE
            book.title
            LIKE :search";
            //WHERE
            //MATCH(book.title)
            //AGAINST(:search IN NATURAL LANGUAGE MODE)";  //for full word search (if type 'du' will search for exact 'du'

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':search', $search, PDO::PARAM_STR);
  $stmt->execute();
  // fetch multiple books
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}







/*
//2 variant with fixed book limit
function getRandomBooks($dbh)
{
  $query = "SELECT
            book.book_id,
            book.title,
            book.image,
            book.num_pages,
            book.year_published,
            book.price,
            book.description,
            author.name as author,
            author.author_id,
            genre.name as genre
            FROM
            book
            JOIN author USING(author_id)
            JOIN genre USING(genre_id)
            ORDER BY RAND()
            LIMIT 5";

  $stmt = $dbh->prepare($query);
  
  $stmt->bindValue(':limit', $limit, PDO::PARAM_INT); // comment it, if use commented lines above
  
  $stmt->execute();
  
  //fetch multiple books
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  
}
*/




