<?php

$title = 'Book Details';

//basic config file
include __DIR__.'/../config/config.php';

//functions
  //functions for sidebar Categories
include __DIR__.'/../model/genre.php';//used by categories.inc.php
  //functions for books
include __DIR__.'/../model/book.php';

//check if no book_id is in URL, send back the books.php (books page)
if(empty($_GET['book_id'])) {
  header('Location: books.php');
  exit; //die
}


//received book_id (from url) is used to get info about specific book which owns that id
$book = getBook($dbh, $_GET['book_id']);

//genre list for side bar (is used in categories.inc.php)
$genres = allGenres($dbh);

//this var is used to display "other books by this publisher" at the bottom of the page
$publisher_books = getBookByPublisher($dbh, $book['publisher_id']);

// use this for first part of flag image name
$flag = strtolower($book['country']);

// use this for first part of author image name
$author = strtolower(str_replace(' ', '_', $book['author']));


// HTML DOCTYPE starts here, always at the bottom, right before closing php "? >"
// DOCTYPE, header with navigation menu
include __DIR__.'/../templates/header.inc.php';
// banner image
include __DIR__.'/../templates/banner.inc.php';
//search panel
include __DIR__.'/../templates/search.inc.php';
?>


  <h1><?php echo $title; ?></h1>

  <!-- sidebar Categories -- -->
  <?php include __DIR__.'/../templates/categories.inc.php' ?>

  <div class="shelf">
    
    <!-- image of the book -->
    <div class="book_cover">
      <img src="images/covers/<?php echo $book['image']?>" alt="<?php echo $book['title']?>" />
    </div><!-- END .book_cover -->
    
    <!-- info about book -->
    <div class="book_details">
      <h3><?php echo $book['title'] ?></h3>
      <ul>
        <li><strong> Title: </strong> <?php echo $book['title']?></li>
        <li><strong> Author: </strong> <?php echo $book['author']?></li>
        <li><strong> Genre: </strong> <?php echo $book['genre']?></li>
        <li><strong> Format: </strong> <?php echo $book['format']?></li>
        <li><strong> Number of Pages: </strong> <?php echo $book['num_pages']?></li>
        <li><strong> Year Published: </strong> <?php echo $book['year_published']?></li>
        <li><strong> In Print: </strong> <?php echo ($book['in_print']) ? 'Yes' : 'No';?></li><!-- if in_print = true then "yes" -->
        <li><strong> Price: </strong> $<?php echo $book['price']?></li>
        <li><strong> Publisher: </strong> <?php echo $book['publisher']?></li>
        <li><strong> Publisher City: </strong> <?php echo $book['city']?></li>
      </ul>
    </div><!-- END.book_details -->


    <!-- "Meet the author" section -->
    <div class="book_author">
      <h4>Meet the author...</h4>
      
      <!-- author name above the image -->
      <h5><?php echo $book['author']?></h5>
      
      <!-- author image -->
      <img src="images/authors/<?php echo $author ?>"
           alt="<?php echo $book['author'] ?>" />
      
      <!-- author name and country -->
      <p>
        <?php echo $book['author']?>. Country: <?php echo $book['country']?>
      </p>
      
      <!-- country flag image -->
      <img src="images/countries/<?php echo $flag; ?>"
           alt="<?php echo $book['country']?> Flag" />
           
      <p>
        View <a href="books.php?author_id=<?php echo $book['author_id']?>">other books by this author</a>.
      </p>
    </div><!-- /.book_author -->


    <!-- description of the book -->
    <div class="book_description">
      <h4>Description</h4>
      <p><?php echo $book['description']?></p>
    </div>


    <div class="book_publisher">
      <h3>Other books by this publisher</h3>
      
      <?php foreach($publisher_books as $row) : ?><!-- loop start -->
        <div class="book_item">
          <a href="detail.php?book_id=<?php echo $row['book_id'];?>">
            <img src="images/covers/<?php echo $row['image']?>" 
                 alt="<?php echo $row['title'];?>" />
          </a>
        </div>
      <?php endforeach;?><!-- loop end -->
      
    </div><!-- end .book_publisher -->


  </div><!-- /.shelf -->

</div><!-- /.container -->

<?php include __DIR__.'/../templates/footer.inc.php'; ?>