  <?php
  
  include __DIR__.'/../config/config.php';
  
  
  //for search field, if we get any keyword value in $_GET
  if(!empty($_GET['keyword'])) {
    $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
    $keyword="%$keyword%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
    //$query for live search, we need only book title and book_id for getting detail info about a book
    $query = "SELECT 
              book.book_id, 
              book.title 
              FROM book 
              WHERE book.title 
              LIKE :keyword 
              LIMIT 5"; //limit results to max 5, search in title (WHERE book.title) by $keyword (binded :keyword)
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':keyword',$keyword); //binding :keyword to the value of $keyword
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //print_r ($result); //to check if correct results appear in array
    if(empty($result)) { // so if we have 0 records associated to keyword display no records found
      echo '<div id="item">No results found!</div>';
    }//END if(empty($result))
    else {
      foreach($result as $row => $innerArray) { //targeting arrays (for every found book: id and title) inside $result array //in this case $innerArray is an array inside $result array
        echo "<a href='detail.php?book_id={$innerArray['book_id']}'> {$innerArray['title']} </a><br />";
      }// END foreach
    }; //END else
  }; //END if(!empty($_GET['keyword']))