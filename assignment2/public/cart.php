<?php

//load config file with session_start() in it
//session_start() must be always first loaded on page
require __DIR__.'/../config/config.php';

//require APP. '/model/cart_model.php';
require __DIR__.'/../model/cart_model.php';

//var_dump($_POST);



//make sure we have a POST request
if($_SERVER['REQUEST_METHOD'] != 'POST') {
  die ('Sorry, you are in the wrong place');
}


//make sure there is a cart session or create one
if(!isset($_SESSION['cart'])) {
  $_SESSION['cart'] = array();
}


if(!empty($_POST['book_id'])) {
  addToCart($dbh, $_POST['book_id']);
  header('Location: ' . $_SERVER['HTTP_REFERER']);
  die;
}























