<?php

//basic config file, includes database path(connection to php), username and password
include __DIR__.'/../config/config.php';

//functions
  //functions for sidebar Categories (genres list)
include __DIR__.'/../model/genre.php';
  //functions for books
include __DIR__.'/../model/book.php';



/*check if any id (author_id to get 
author info or genre_id from sidebar to get list of books in genre) is sent in URL*/

//if author_id is found in $_GET url (sent by clicking on author name link)
if(!empty($_GET['author_id'])) {
  $books = getBooksByAuthor($dbh, $_GET['author_id']);
  $title = 'Books by author: ' . $books[0]['author'];
} 
//if genre_id is found (sent by clicking on a genre name in Categories sidebar)
elseif (!empty($_GET['genre_id'])) {
  $books = getBooksByGenre($dbh, $_GET['genre_id']);
  $title = 'Books by genere: ' . $books[0]['genre'];
  var_dump($books);
} 
// if have $_GET['keyword'] ('keyword' is the name of search field <input name="keyword" />)
// this $_GET['keyword'] value comes to this page only if a user presses search button or enter in input search field
######## IMPORTANT ##########
// the same $_GET['keyword'] is sent when a user types in the search field and gets AJAX live search results. but $_GET['keyword'] is sent to search.php
  // so we cant have on this page code for regular search and code for live search at the same time on the same page, because they both use $_GET['keyword']
######## END IMPORTANT #########
// the GET array with serach request looks like $_GET['keyword'] = 'search words'
elseif(!empty($_GET['keyword'])) {
    $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
    $keyword="%$keyword%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
    //echo $keyword;
    //echo "<br/>";
    $books = search($dbh, $keyword); //send to the search function: database and value of $keyword
    $title = 'You searched for: ' . htmlspecialchars($_GET['keyword']);
    //echo $_GET['keyword'];
    //var_dump($books);
}
//if no id is found (default page)
else {
  $books = getRandomBooks($dbh, 5); // 5 - determines max number of books that will be dispayed as suggested
  $title = 'Books suggested for you...';
}



//genre list for side bar (is used in categories.inc.php)
$genres = allGenres($dbh);
//var_dump($genres); //to check if allGenres function works properly

// HTML DOCTYPE starts here, always at the bottom, right before closing php "? >"
// DOCTYPE, header with navigation menu
include __DIR__.'/../templates/header.inc.php';
// banner image
include __DIR__.'/../templates/banner.inc.php';
//search panel
include __DIR__.'/../templates/search.inc.php';
?>

  
  <h1><?php echo $title; ?></h1>
  
  <!-- sidebar Categories -->
  <?php include __DIR__.'/../templates/categories.inc.php'; ?>


  <!--START book info -------------------------------------------->
  <!-------------------------------------------------------------->
  <div class="shelf">
    <?php if(empty($books)){echo "<h1>Nothing found!</h1>";} ?><!-- this message is for no results for search -->
    <?php foreach($books as $row) : ?><!-- loop start -->
      <div class="book">
        
        <!-- book image -->
        <div class="img">
          <img src="images/covers/<?php echo $row['image']?>"
               alt="<?php echo $row['title']?>" />
        </div>
        
        <div class="details">
          <p>
            <!--book title in bold-->
            <strong><?php echo $row['title']?></strong><br />
            
            by <!-- Author name link (sends in the request url author_id to get author info)-->
            <a href="books.php?author_id=<?php echo $row['author_id']?>">
              <?php echo $row['author']?>
            </a><br />
            
            <!-- genre, pages, year, price -->
            <span><?php echo $row['genre']?></span>, <?php echo $row['num_pages']?> pages, <?php echo $row['year_published']?>, $<?php echo $row['price']?>
          </p>
          
          <!-- description paragraph -->
          <?php echo $row['description']?><!-- <p> tags is already exist in description column in database -->
          
          <!-- "More info" link (sends book_id in the request link to get book info)-->
          <!-- uses getBook function. the function is in book.php file -->
          <p>
            <a class="more" href="detail.php?book_id=<?php echo $row['book_id']?>">
              More info
            </a>
          </p>
        </div><!-- END class="details" -->
        
      </div><!-- /.book -->
    <?php endforeach ?><!-- loop end -->
  </div><!-- /.shelf -->
  <!---------------------------------------------------------->
  <!-- END book info ----------------------------------------->


</div><!-- /.container -->

<!-- footer -->
<?php include __DIR__.'/../templates/footer.inc.php'; ?>