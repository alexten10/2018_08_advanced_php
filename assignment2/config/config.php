<?php


//default config file
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

//start output buffering
ob_start();

//Start a session
session_start();



define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');
define('DB_DSN', 'mysql:host=localhost;dbname=booksite');

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


