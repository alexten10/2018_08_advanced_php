<!-- search field -->
<div class="search">
  <form action="books.php" 
        id="search_form"
        method="get" 
        novalidate="novalidate" 
        autocomplete="off">
        
    <input type="search"
           id="searchbox" 
           name="keyword" 
           maxlength="255" 
           placeholder="Search book by title" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
           
    <input type="submit" value="search" />
    
    <div id="results"></div><!-- live search results will appear here -->
    
  </form>
  
  
</div>
<hr class="clear" />