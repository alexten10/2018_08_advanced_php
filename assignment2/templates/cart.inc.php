<div class="cart">
  <p>
    <strong>your cart:</strong><br />
    <small>you have 1 item in your cart</small><br />
    <small><?php echo $_SESSION['cart']['title'] ?>, 
             by <?php echo $_SESSION['cart']['author'] ?>.
               $<?php echo $_SESSION['cart']['price'] ?>
    </small><br />
    <small><a href="checkout.php">checkout now</a></small>
  </p>
</div>