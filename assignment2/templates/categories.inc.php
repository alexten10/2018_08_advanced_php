<div class="categories">
  <h3>Categories</h3>
  <ul>
    <?php foreach($genres as $row) : ?>
      <li>
        <!-- sends genre_id in the request url to get all books by genre -->
        <!-- uses getBooksByGenre function. Function is in book.php file -->
        <a href="books.php?genre_id=<?php echo $row['genre_id']?>">
          <?php echo $row['name'] ?>
        </a>
      </li>
    <?php endforeach ?>
  </ul>
</div><!-- END class="categories" -->