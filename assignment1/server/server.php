<?php

//server.php
//open this file separately to check if $query works properly, all errors associated with database query will be shown on THIS page only

/*
//check if non- Ajax request comes, die with error message
$headers = getallheaders();
//var_dump($headers);
if (!array_key_exists('X-Requested-With', $headers)) {
  die ('wrong request');
}
*/

//same as above
//check if non- Ajax request comes, die with error message
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
  die('invalid request');
}


define ('APP', __DIR__ . '/..'); //one folder up
require APP . '/config.php'; // sets errors to be displayed (both php and DB errors), defines DB (+ user and password), connects to DB


if(!empty($_GET['book_id'])) { //if have GET request with book_id
  //get query to database for detail view about the book based on book_id
  $query = 'SELECT
            book.book_id,
            book.image,
            book.title,
            book.num_pages,
            book.price,
            book.year_published,
            book.in_print,
            author.name as author,
            publisher.name as publisher,
            format.name as format,
            genre.name as genre
            FROM
            book
            JOIN author USING(author_id)
            JOIN publisher USING(publisher_id)
            JOIN format USING(format_id)
            JOIN genre USING(genre_id)
            WHERE
            book.book_id = :book_id';
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':book_id', $_GET['book_id'], PDO::PARAM_INT);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);// fetch one book info
}

else{
//query for books list column
$query = 'SELECT
          book.book_id,
          book.title
          FROM
          book';

$stmt = $dbh->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);//fetch multiple results
}

header('Content-type: application/json');//if miss this line, index_jquery.html will not work!
echo json_encode($result); //converts a PHP value into a JSON value





